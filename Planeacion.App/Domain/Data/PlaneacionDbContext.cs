﻿using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Model.LineaAgg;
using Planeacion.App.Domain.Model.MantenimientoAgg;
using Planeacion.App.Domain.Model.MaquinaAgg;
using Planeacion.App.Domain.Model.MaterialAgg;
using Planeacion.App.Domain.Model.PlantaAgg;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.EfCore2.Data;
using Vesta2.EfCore2.Utils;
using Vesta2.SequentialDb.Ef2;

namespace Planeacion.App.Domain.Data
{
    public class PlaneacionDbContext : DataContext
    {
        public PlaneacionDbContext(DbContextOptions<PlaneacionDbContext> options) : base(options)
        {
        }
        #region DbSets
        public virtual DbSet<SequentialEntity> SequentialEntity { get; set; }

        public virtual DbSet<TipoMantenimiento> TipoMantenimiento { get; set; }

        public virtual DbSet<Mantenimiento> Mantenimiento { get; set; }

        public virtual DbSet<Maquina> Maquina { get; set; }


        public virtual DbSet<BaseSemiterminado> BaseSemiterminado { get; set; }
        

        public virtual DbSet<IngredienteBase> IngredienteBase { get; set; }

        public virtual DbSet<IngredienteSemiterminado> IngredienteSemiterminado { get; set; }
        public virtual DbSet<Linea> Linea { get; set; }


        public virtual DbSet<TipoLinea> TipoLinea { get; set; }
        
        public virtual DbSet<Planta> Planta { get; set; }

        public virtual DbSet<CategoriaSKU> CategoriaSKU { get; set; }

        public virtual DbSet<Categoria> Categoria { get; set; }

        public virtual DbSet<CategoriaSkuPorPlanta> CategoriaSkuPorPlanta { get; set; }

        public virtual DbSet<Ingrediente> Ingrediente { get; set; }

        public virtual DbSet<Base> Base { get; set; }

        public  virtual DbSet<Semiterminado> Semiterminado { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.SetVestaOnModelCreating();

        }
    }
}