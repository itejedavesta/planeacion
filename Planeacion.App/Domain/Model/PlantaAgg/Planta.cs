﻿using FluentValidation;
using Planeacion.App.Domain.Model.LineaAgg;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.PlantaAgg
{
    public class Planta : Entity
    {

        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion


        #region navs

        public virtual ICollection<Linea> Lineas { get; set; }
        public virtual ICollection<CategoriaSkuPorPlanta> CategoriasSkuPorPlanta { get; set; }
        #endregion

        #region properties

        public string Nombre { get; set; }
        public Guid SitioPersonaId { get; set; }
        #endregion


        #region publicmethods

        public Planta()
        {
            Lineas = new HashSet<Linea>();
            CategoriasSkuPorPlanta = new HashSet<CategoriaSkuPorPlanta>();
        }

        public static Planta New(string nombre, Guid sitiopersonaid, Guid usuarioId)
        {
            var newplante = new Planta
            {
                Nombre = nombre,
                SitioPersonaId = sitiopersonaid,
                CreatedBy = usuarioId,

            };
            return newplante;
        }
        #endregion
    }



    public class PlantaValidator : AbstractValidator<Planta>
    {
        public PlantaValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.Nombre).NotEmpty();
            RuleFor(s => s.SitioPersonaId).NotEmpty();
        }

    }
}
