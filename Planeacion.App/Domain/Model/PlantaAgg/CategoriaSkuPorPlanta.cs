﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.PlantaAgg
{
    public class CategoriaSkuPorPlanta : Entity
    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion

        #region properties
        public int Dias { get; set; }

        #endregion
        #region foreignkey
        public virtual Planta Planta { get; private set; }
        public Guid PlantaId { get; private set; }

        public virtual CategoriaSKU CategoriaSKU { get; private set; }
        public Guid CategoriaSKUId { get; private set; }
        #endregion

        #region publicmethods

        public static CategoriaSkuPorPlanta New(Guid planteid, Guid categoriaskuid, Guid usuarioId)
        {
            var newcategoriasku = new CategoriaSkuPorPlanta
            {
                PlantaId = planteid,
                CategoriaSKUId = categoriaskuid,
                CreatedBy = usuarioId
            };
            return newcategoriasku;
        }
        #endregion
    }



    public class CategoriaSkuPorPlantaValidator : AbstractValidator<CategoriaSkuPorPlanta>
    {
        public CategoriaSkuPorPlantaValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.CategoriaSKUId).NotEmpty();
            RuleFor(s => s.CategoriaSKUId).NotEmpty();



        }

    }
}
