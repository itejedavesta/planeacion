﻿using FluentValidation;
using Planeacion.App.Domain.Model.PlantaAgg;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.PlantaAgg
{
public class CategoriaSKU:Entity
    {

        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion


        #region properties
        public string Tipo { get; set; }

        #endregion

        #region foreignkey
        public virtual ICollection<CategoriaSkuPorPlanta> CategoriaSkuPorPlantas { get; private set; }

        #endregion

        #region publicmethods

        public CategoriaSKU()
        {
            CategoriaSkuPorPlantas = new HashSet<CategoriaSkuPorPlanta>();

        }
        
        public static CategoriaSKU New(string tipo, Guid usuarioId)
        {
            var newcategoria = new CategoriaSKU
            {
                Tipo = tipo,
                CreatedBy = usuarioId
            };
            return newcategoria;
        }
        #endregion
    }
}


public class CategoriaSkuValidator : AbstractValidator<CategoriaSKU>
{
    public CategoriaSkuValidator()
    {
        RuleFor(s => s.Id).NotEmpty();
        RuleFor(s => s.CreatedBy).NotEmpty();
        RuleFor(s => s.Tipo).NotEmpty();
      
    }

}