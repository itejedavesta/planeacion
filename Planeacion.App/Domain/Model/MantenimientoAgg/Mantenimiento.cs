﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MantenimientoAgg
{
    public class Mantenimiento:Entity


    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion

        #region properties
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }

        public string Observacion { get; set; }
        #endregion
        #region foreignkey
        public virtual TipoMantenimiento TipoMantenimiento { get; set; }
        public Guid TipoMantenimientoId { get; set; }

        #endregion

        #region publicmethods
  
        public static Mantenimiento New (DateTime inicio, DateTime fin,string observacion, Guid tipomantenimientoId, Guid createdby)
        {
            var newmantenimiento = new Mantenimiento
            {
                Inicio = inicio,
                Fin = fin,
                Observacion = observacion,
                TipoMantenimientoId = tipomantenimientoId,
                CreatedBy = createdby
            };
            return newmantenimiento;
            
        }
        #endregion
    }

    public class MantenimientoValidator : FluentValidation.AbstractValidator<Mantenimiento>
    {
        public MantenimientoValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.Inicio).NotEmpty();
            
        }
    }

        }
