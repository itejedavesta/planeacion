﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MantenimientoAgg
{
    public class TipoMantenimiento:Entity
    {
        #region properties
        public string Nombre { get; set; }

        #endregion

        #region navs
        public virtual ICollection<Mantenimiento> Mantenimientos { get; private set; }
        #endregion
        #region publicmethods


        public TipoMantenimiento()
        {
            Mantenimientos = new HashSet<Mantenimiento>();
        }
        
        public static TipoMantenimiento New(string nombre, Guid createdby)
        {
            var newtipomantenimiento = new TipoMantenimiento
            {
                Nombre = nombre,
                CreatedBy=createdby
            };
            return newtipomantenimiento;
        }
        #endregion

    }


    public class TipoMantenimientoValidator : FluentValidation.AbstractValidator<TipoMantenimiento>
    {
        public TipoMantenimientoValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.Nombre).NotEmpty();

        }
    }

}
