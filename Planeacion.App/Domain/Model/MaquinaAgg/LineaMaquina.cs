﻿using Planeacion.App.Domain.Model.LineaAgg;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MaquinaAgg
{
    public class LineaMaquina:Entity
    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion
        #region foreignkey

        public virtual Linea Linea { get; private set; }
        public Guid LineaId { get; private set; }

        public virtual Maquina Maquina { get; private set; }
        public Guid MaquinaId { get; private set; }


        #endregion

        #region publicmethods
        public static LineaMaquina New(Guid lineaid, Guid maquinaid, Guid usuarioId)
        {
            var newlineamaquina = new LineaMaquina
            {
                LineaId = lineaid,
                MaquinaId = maquinaid,
                CreatedBy = usuarioId
            };
            return newlineamaquina;
        }
        #endregion
    }
}
