﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MaquinaAgg
{
    public class Maquina:Entity 
    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion
        #region properties
        public string Codigo { get; set; }

        public string Descripcion { get; set; }
        public string Numero { get; set; }
        #endregion

        #region navs
        public virtual ICollection<LineaMaquina> Lineas { get; private set; }
        #endregion

        #region foreignkey

        #endregion

        #region publicmethods
        public Maquina()
        {
            Lineas = new HashSet<LineaMaquina>();
        }
     
        public static Maquina New(string codigo,string descripcion,string numero, Guid usuarioId)
        {
            var newmaquina = new Maquina
            {
                Codigo = codigo,
                Descripcion = descripcion,
                Numero = numero,
                CreatedBy = usuarioId
            }; return newmaquina;

        }
        #endregion
    }


    public class MaquinaValidator : FluentValidation.AbstractValidator<Maquina>
    {
        public MaquinaValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.Codigo).NotEmpty();
            RuleFor(s => s.Descripcion).NotEmpty();
            RuleFor(s => s.Numero).NotEmpty();

        }
    }

}
