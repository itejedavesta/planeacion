﻿using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MaquinaAgg
{
    public class Productividad:Entity
    {
        public double Velocidad { get; set; }
        public double Capacidad { get; set; }
        
        #region navs
        public virtual ICollection<Maquina> Maquinas { get; private set; }

        #endregion
        #region publicmethods
        public static Productividad New(double vel, double cap)
        {
            var newproductividad = new Productividad
            {
                Capacidad = cap,
                Velocidad = vel
            };
            return newproductividad;
        }

        public Productividad()
        {
            Maquinas = new HashSet<Maquina>();
        }
        #endregion
    }
}
