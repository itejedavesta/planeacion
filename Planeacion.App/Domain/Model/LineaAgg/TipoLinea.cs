﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.LineaAgg
{
    public class TipoLinea : Entity
    {

        #region properties
        public string Descripcion { get; set; }
        #endregion

        #region navs
        public virtual ICollection<Linea> Lineas { get; set; }

        #endregion

        #region publicmethods

        public TipoLinea()
        {
            Lineas = new HashSet<Linea>();
        }


        public static TipoLinea New(string descripcion, Guid usuarioId)
        {
            var newtipolinea = new TipoLinea
            {
                Descripcion = descripcion,
                CreatedBy = usuarioId
            };
            return newtipolinea;
        }
        #endregion
    }

    public class TipoLineaValidator : AbstractValidator<TipoLinea>
    {
        public TipoLineaValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.Descripcion).NotEmpty();
       
        }

    }
}
