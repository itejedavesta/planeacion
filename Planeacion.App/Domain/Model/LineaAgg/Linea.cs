﻿using Planeacion.App.Domain.Model.MaquinaAgg;
using System;
using System.Collections.Generic;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.LineaAgg
{
    public class Linea:Entity
    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion

        #region properties
        public string LineaDescripcion { get; set; }
        public string Turno { get; set; }
        public int Orden { get; set; }

        #endregion
        #region foreingkey
        public virtual TipoLinea TipoLinea { get; set; }
        public Guid TipoLineaId { get; set; }
        #endregion

        #region navs
        public virtual ICollection<LineaMaquina> Maquinas { get; private set; }

        #endregion

        #region publicmethods
        public Linea()
        {
            Maquinas = new HashSet<LineaMaquina>();
        }

        public static Linea New(string lineadescp,string turno, int orden,Guid tipolineaid, Guid usuarioId)
        {
            var newlinea = new Linea
            {
                CreatedBy = usuarioId,
                LineaDescripcion = lineadescp,
                Turno = turno,
                Orden = orden
            };
            return newlinea;
        }
        #endregion

    }
}
