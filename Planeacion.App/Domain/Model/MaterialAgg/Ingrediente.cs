﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planeacion.App.Domain.Model.MaterialAgg
{
    public class Ingrediente:Material
    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion


        #region properties
        public string Descripcion { get; set; }
        public Guid SkuId { get; set; }
        #endregion

        #region navs

        public virtual ICollection<IngredienteBase> Bases { get; private set; }
       
        public virtual ICollection<IngredienteSemiterminado> Semiterminados { get; private set; }
        #endregion

        #region publicmethods

        public Ingrediente()
        {
            Bases = new HashSet<IngredienteBase>();
            Semiterminados = new HashSet<IngredienteSemiterminado>();
        }


        public static Ingrediente New(string descripcion, Guid skuid,Guid categoriaId, Guid usuarioId)
        {
            var newingrediente = new Ingrediente
            {
                Descripcion = descripcion,
                SkuId = skuid,
                CreatedBy = usuarioId
            };
            newingrediente.SetCategoriaId(categoriaId);
            return newingrediente;
        }
        #endregion
    }


    public class IngredienteValidator : AbstractValidator<Ingrediente>
    {
        public IngredienteValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.Descripcion).NotEmpty();
            RuleFor(s => s.CategoriaId).NotEmpty();

        }

    }
}
