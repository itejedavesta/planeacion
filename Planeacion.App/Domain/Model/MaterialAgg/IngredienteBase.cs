﻿using Planeacion.App.Domain.Model.MaquinaAgg;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MaterialAgg
{
    public class IngredienteBase : Entity
    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }


        #endregion

        #region foreignkey

        public virtual Ingrediente Ingrediente { get; private set; }
        public Guid IngredienteId { get; private set; }

        public virtual Base Base { get; private set; }
        public Guid BaseId { get; private set; }

        #endregion


        #region publicmethods
        public static IngredienteBase New(Guid ingredienteid, Guid baseid, Guid usuarioId)
        {
            var newingredientebase = new IngredienteBase
            {
                BaseId = baseid,
                IngredienteId = ingredienteid,
                CreatedBy = usuarioId
            };
            return newingredientebase;
        }
        #endregion

    }
}
