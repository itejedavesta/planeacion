﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planeacion.App.Domain.Model.MaterialAgg
{
    public class Semiterminado:Material
    {

        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion

        #region properties
        public string NombreBase { get; set; }
        public Guid SkuId { get; set; }
        public string Nombre { get; set; }
        #endregion


        #region navs
     
        public Semiterminado()
        {

        }
        #endregion

        #region publicmethods


        public static Semiterminado New(string nombrebase,string nombre, Guid skuid,Guid categoriaId, Guid usuarioid)
        {
            var newsemiterminado = new Semiterminado
            {
                Nombre = nombre,
                NombreBase = nombrebase,
                SkuId = skuid,
                CreatedBy=usuarioid
            };
            newsemiterminado.SetCategoriaId(categoriaId);
            return newsemiterminado;
        }


        #endregion

    }

    public class SemiterminadoValidator : AbstractValidator<Semiterminado>
    {
        public SemiterminadoValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.Nombre).NotEmpty();
            RuleFor(s => s.NombreBase).NotEmpty();

            RuleFor(s => s.CategoriaId).NotEmpty();

        }

    }
}
