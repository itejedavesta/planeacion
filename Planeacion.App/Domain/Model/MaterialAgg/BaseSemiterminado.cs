﻿using Planeacion.App.Domain.Model.MaquinaAgg;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MaterialAgg
{
    public class BaseSemiterminado : Entity
    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }

        #endregion

        #region foreignkey
        public virtual Base Base { get; private set; }
        public Guid BaseId { get; private set; }

        public virtual Semiterminado Semiterminado { get; private set; }
        public Guid SemiterminadoId { get; private set; }
        #endregion
        #region properties

        #endregion


        #region publicmethods
        public static BaseSemiterminado New(Guid baseid, Guid semiterminadoId, Guid usuarioId)
        {
            var newbase = new BaseSemiterminado
            {
                BaseId = baseid,
                SemiterminadoId = semiterminadoId,
                CreatedBy = usuarioId
            };
            return newbase;
        }
        
        #endregion
    }
}
