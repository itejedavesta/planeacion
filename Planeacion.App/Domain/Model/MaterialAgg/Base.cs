﻿using FluentValidation;
using Planeacion.App.Domain.Model.MaterialAgg;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MaquinaAgg
{
    public class Base: Material
    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion


        #region properties

        public string NombreBase { get; set; }
        public string Nombre { get; set; }
        #endregion
        #region navs
        public virtual ICollection<Productividad> Productividad { get; private set; }
        public virtual ICollection<IngredienteBase> Ingredientes { get; private set; }
        public virtual ICollection<BaseSemiterminado> Semiterminados { get; private set; }
        #endregion

        #region publicmethods


        public static Base New(string nombre, string nombrebase,Guid categoriaid, Guid usuarioId)
        {
            var newbase = new Base
            {
                Nombre = nombre,
                NombreBase = nombrebase,
                CreatedBy = usuarioId
            };
            newbase.SetCategoriaId(categoriaid);
            return newbase;
        }


        public Base()
        {
            Productividad = new HashSet<Productividad>();
            Ingredientes = new HashSet<IngredienteBase>();
            Semiterminados = new HashSet<BaseSemiterminado>();
        }
        #endregion
    }


    public class BaseValidator : AbstractValidator<Base>
    {
        public BaseValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.NombreBase).NotEmpty();
            RuleFor(s => s.Nombre).NotEmpty();

            RuleFor(s => s.CategoriaId).NotEmpty();

        }

    }
}
