﻿using FluentValidation;
using Planeacion.App.Domain.Model.MaterialAgg;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MaterialAgg
{
public  class IngredienteSemiterminado:Entity
    {

        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }


        #endregion

        #region foreignkey

        public virtual Ingrediente Ingrediente { get; private set; }
        public Guid IngredienteId { get; private set; }

        public virtual Semiterminado Semiterminado { get; private set; }

        public Guid SemiterminadoId { get; private set; }
        #endregion


        #region publicmethods

        public static IngredienteSemiterminado New(Guid ingredienteid,Guid semiterminadoid, Guid usuarioId)
        {
            var newingrediente = new IngredienteSemiterminado
            {
                IngredienteId = ingredienteid,
                SemiterminadoId = semiterminadoid,
                CreatedBy = usuarioId
            };

            return newingrediente;
        }

        #endregion

    }
    public class IngredienteSemiterminadoValidator : AbstractValidator<IngredienteSemiterminado>
    {
        public IngredienteSemiterminadoValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.IngredienteId).NotEmpty();
            RuleFor(s => s.SemiterminadoId).NotEmpty();

        }

    }
}

