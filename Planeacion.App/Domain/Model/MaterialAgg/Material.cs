﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.MaterialAgg
{
    public class Material:Entity
    {
        //#region properties
        //public string Descripcion { get; set; }
        //public Guid SkuId { get; set; }
        //#endregion


        #region foreignkey

        public virtual Categoria Categoria { get; private set; }
        public Guid CategoriaId { get; private set; }
        #endregion


        #region publicmethods

        public void SetCategoriaId(Guid categoriaId)
        {
            CategoriaId = categoriaId;
        }

        #endregion

    }

    public class Categoria:Entity
    {
        #region Members

        bool _isEnabled = true;

        #endregion
        #region propertieenabled


        public bool Enabled
        {
            get { return _isEnabled; }
            private set { _isEnabled = value; }
        }

        #endregion


        #region enabledisable
        public void Enable()
        {
            if (!Enabled)
            {
                this._isEnabled = true;

            }
        }

        public void Disable()
        {
            if (Enabled)
            {
                this._isEnabled = false;

            }
        }
        #endregion

        #region properties
        public string Tipo { get; set; }
        #endregion

        #region foreignkey

        public virtual Material Material { get; private set; }
        public Guid MaterialId { get; private set; }

        #endregion

        #region publicmethods
        
        public static Categoria New(string tipo, Guid usuarioId)
        {
            var newcategoria = new Categoria
            {
                Tipo = tipo,
                CreatedBy = usuarioId
            };
            return newcategoria;
        }

        #endregion

    }


    public class CategoriaValidator : AbstractValidator<Categoria>
    {
        public CategoriaValidator()
        {
            RuleFor(s => s.Id).NotEmpty();
            RuleFor(s => s.CreatedBy).NotEmpty();
            RuleFor(s => s.Tipo).NotEmpty();

        }

    }
}
