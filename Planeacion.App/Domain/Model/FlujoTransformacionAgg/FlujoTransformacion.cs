﻿using Planeacion.App.Domain.Model.MaterialAgg;
using System;
using System.Collections.Generic;
using System.Text;
using Vesta2.Core.Domain;

namespace Planeacion.App.Domain.Model.FlujoTransformacionAgg
{
    public class FlujoTransformacion:Entity
    {
        public string TransformacionDescripcion { get; set; }
        public Guid Linea { get; set; }

        public string LoteMinimo { get; set; }
        public string Orden { get; set;}
        public string Capacidad { get; set; }

        #region navs
        public virtual ICollection<Material> Materiales { get; set; }
        #endregion
    }
}
