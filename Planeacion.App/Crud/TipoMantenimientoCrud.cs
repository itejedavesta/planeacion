﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.MantenimientoAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
  public  class TipoMantenimientoCrud
    {

        public class TipoMantenimientoVm

        {

            public Guid Id { get; set; }
            public string Mantenimiento { get; set; }

        }
        public class Create
        {
            
            public class Command:IRequest<TipoMantenimientoVm>
            {
                public string Nombre { get; set; }
                public Guid CreatedBy { get; set; }
            }

            public class CommandHandler : IRequestHandler<Command, TipoMantenimientoVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<TipoMantenimientoVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var tipomantenimiento = TipoMantenimiento.New(command.Nombre,command.CreatedBy);
                    await _context.TipoMantenimiento.AddAsync(tipomantenimiento);
                    await _context.SaveChangesAsync();
                    return tipomantenimiento.Adapt<TipoMantenimientoVm>();
                }
            }
        }


        public class Index

        {

            public class Query:IRequest<List<TipoMantenimientoVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<TipoMantenimientoVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<TipoMantenimientoVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var tipomantenimientos = await _context.TipoMantenimiento
                        .Where(x => !x.IsSoftDeleted).ProjectToType<TipoMantenimientoVm>().ToListAsync();
                    return tipomantenimientos;
                }
            }
        }
    }
}
