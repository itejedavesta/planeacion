﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.MaterialAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.Core.Extensions;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
public   class SemiterminadoCrud
    {
        public class Create
        {
            public class    SemiterminadoVm
            {
                public Guid Id { get; set; }
                public Guid CategoriaId { get; set; }
                public string NombreBase { get; set; }
                public string Nombre { get; set; }
                public Guid SkuId { get; set; }

            }
            public class Command : IRequest<SemiterminadoVm>
            {
                public Guid CategoriaId { get; set; }
                public string NombreBase { get; set; }
                public string Nombre { get; set; }

                public Guid SkuId { get; set; }
                public Guid UsuarioId { get; set; }
            }

            public class CommandHandler : IRequestHandler<Command, SemiterminadoVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<SemiterminadoVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var categoria = await _context.Categoria.Where(x => x.Id == command.CategoriaId && x.Enabled &&!x.IsSoftDeleted)
                        .FirstOrDefaultAsync();
                    categoria.ThrowIfNull("Categoria");
                    var newbase = Semiterminado.New(command.NombreBase,command.Nombre,command.SkuId,command.CategoriaId,command.UsuarioId);
                    await _context.Semiterminado.AddAsync(newbase);
                    await _context.SaveChangesAsync();
                    return newbase.Adapt<SemiterminadoVm>();
                }
            }
        }

        public class Index
        {

            public class SemiterminadoVm
            {
                public Guid Id { get; set; }
                public Guid CategoriaId { get; set; }
                public string NombreBase { get; set; }
                public string Nombre { get; set; }
                public Guid SkuId { get; set; }

            }
            public class Query : IRequest<List<SemiterminadoVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<SemiterminadoVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<SemiterminadoVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var bases = await _context.Semiterminado
                        .Where(x => !x.IsSoftDeleted && x.Enabled).ProjectToType<SemiterminadoVm>().ToListAsync();
                    return bases;
                }
            }
        }
    }
}
