﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.MaquinaAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class MaquinaCrud
    {

        public class MaquinaVm
        {

            public Guid Id { get; set; }
            public string Codigo { get; set; }

            public string Descripcion { get; set; }

            public string Numero { get; set; }

            public Guid CreatedBy { get; set; }
        }
        public class Create
        {
            public class MaquinaVm
            {

                public Guid Id { get; set; }
                public string Codigo { get; set; }

                public string Descripcion { get; set; }

                public string Numero { get; set; }

                public Guid CreatedBy { get; set; }
            }
            public class Command:IRequest<MaquinaVm>
            {
                public string Codigo { get; set; }

                public string Descripcion { get; set; }

                public string Numero { get; set; }

                public Guid UsuarioId { get; set; }
            }
            public class CommandHandler : IRequestHandler<Command, MaquinaVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<MaquinaVm> Handle(Command command, CancellationToken cancellationToken)
                {
                
                    var maquina = Maquina.New(command.Codigo, command.Descripcion, command.Numero, command.UsuarioId);
                    await _context.Maquina.AddAsync(maquina);
                    await _context.SaveChangesAsync();
                    return maquina.Adapt<MaquinaVm>();
                }
            }

        }

        public class Index

        {

           
            public class Query : IRequest<List<MaquinaVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<MaquinaVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<MaquinaVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var maquinas = await _context.Maquina
                        .Where(x => !x.IsSoftDeleted && x.Enabled).ProjectToType<MaquinaVm>().ToListAsync();
                    return maquinas;
                }
            }
        }
    }
}
