﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.MantenimientoAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.Core.Extensions;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class MantenimientoCrud
    {

        public class Create
        {


            public class MantenimientoVm
            {
                public Guid Id { get; set; }
                public DateTime Inicio { get; set; }
                public DateTime? Fin { get; set; }

                public Guid TipoMantenimientoId { get; set; }
                public string Observacion { get; set; }
            }
            public class Command:IRequest<MantenimientoVm>
            {
                public DateTime Inicio { get; set; }
                public DateTime Fin { get; set; }

                public Guid TipoMantenimientoId { get; set; }
                public string Observacion { get; set; }
                public Guid CreatedBy { get; set; }

            }
            public class CommandHandler : IRequestHandler<Command, MantenimientoVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<MantenimientoVm> Handle(Command command, CancellationToken cancellationToken)
                {
                    var tipomantenimiento = await _context.TipoMantenimiento.Where(c => c.Id == command.TipoMantenimientoId && c.IsSoftDeleted).FirstOrDefaultAsync();
                    tipomantenimiento.ThrowIfNull("TipoMantenimiento");

                    var mantenimiento = Mantenimiento.New(command.Inicio,command.Fin,command.Observacion, command.TipoMantenimientoId,command.CreatedBy);
                    await _context.Mantenimiento.AddAsync(mantenimiento);
                    await _context.SaveChangesAsync();
                    return mantenimiento.Adapt<MantenimientoVm>();
                }
            }

        }

        public class Index

        {

            public class MantenimientoVm
            {
                public Guid Id { get; set; }
                public DateTime Inicio { get; set; }
                public DateTime? Fin { get; set; }

                public Guid TipoMantenimientoId { get; set; }
                public string Observacion { get; set; }
            }

            public class Query : IRequest<List<MantenimientoVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<MantenimientoVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<MantenimientoVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var tipomantenimientos = await _context.Mantenimiento
                        .Where(x => !x.IsSoftDeleted).ProjectToType<MantenimientoVm>().ToListAsync();
                    return tipomantenimientos;
                }
            }
        }
    }
}
