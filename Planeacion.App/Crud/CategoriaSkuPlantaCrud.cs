﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.PlantaAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.Core.Extensions;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
   public class CategoriaSkuPlantaCrud

    {
        public class Create
        {

            public class CategoriaSkuPorPlantaVm
            {
                public Guid Id { get; set; }
                public Guid PlantaId { get; set; }
                public Guid CategoriaSKUId { get; set; }
              
            }
            public class Command : IRequest<CategoriaSkuPorPlantaVm>
            {

                public Guid UsuarioId { get; set; }
                public Guid PlantaId { get; set; }
                public Guid CategoriaSKUId { get; set; }

            }
            public class CommandHandler : IRequestHandler<Command, CategoriaSkuPorPlantaVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<CategoriaSkuPorPlantaVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var categoria = await _context.CategoriaSKU.Where(x => x.Id == command.CategoriaSKUId && x.Enabled && !x.IsSoftDeleted).FirstOrDefaultAsync();
                    categoria.ThrowIfNull("Categoria");
                    var planta = await _context.Planta.Where(x => x.Id == command.PlantaId && x.Enabled && !x.IsSoftDeleted)
                        .FirstOrDefaultAsync();
                    planta.ThrowIfNull("Planta");

                    var newcategoriaskuplanta = CategoriaSkuPorPlanta.New(planta.Id, categoria.Id, command.UsuarioId);
                    await _validationManager.ValidateAndThrow(newcategoriaskuplanta, RulesSet.Default);
                    await _context.CategoriaSkuPorPlanta.AddAsync(newcategoriaskuplanta);
                    await _context.SaveChangesAsync();

                    return newcategoriaskuplanta.Adapt<CategoriaSkuPorPlantaVm>(); ;

                }
            }

        }

        public class Index

        {
            public class CategoriaSkuPorPlantaVm
            {
                public Guid Id { get; set; }
                public Guid PlantaId { get; set; }
                public Guid CategoriaSKUId { get; set; }

            }

            public class Query : IRequest<List<CategoriaSkuPorPlantaVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<CategoriaSkuPorPlantaVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<CategoriaSkuPorPlantaVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var categoriaporsku = await _context.CategoriaSkuPorPlanta
                        .Where(x => !x.IsSoftDeleted && x.Enabled).ProjectToType<CategoriaSkuPorPlantaVm>().ToListAsync();
                    return categoriaporsku;
                }
            }
        }
    }


}
