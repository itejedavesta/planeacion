﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.MaterialAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class BaseSemiterminadoCrud
    {

        public class Create
        {
            public class BaseSemiterminadoVm
            {
                public Guid Id { get; set; }
                public Guid BaseId { get; set; }
                public Guid SemiterminadoId { get; set; }

            }
            public class Command:IRequest<BaseSemiterminadoVm>
            {
                public Guid BaseId { get; set; }
                public Guid SemiterminadoId { get; set; }

                public Guid UsuarioId { get; set; }
            }

            public class CommandHandler : IRequestHandler<Command, BaseSemiterminadoVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<BaseSemiterminadoVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var basesemiterminado = BaseSemiterminado.New(command.BaseId, command.SemiterminadoId, command.UsuarioId);
                    await _context.BaseSemiterminado.AddAsync(basesemiterminado);
                    await _context.SaveChangesAsync();
                    return basesemiterminado.Adapt<BaseSemiterminadoVm>();
                }
            }

        }


        public class Index
        {
            public class BaseSemiterminadoVm
            {
                public Guid Id { get; set; }
                public Guid BaseId { get; set; }
                public Guid SemiterminadoId { get; set; }

            }
            public class Query : IRequest<List<BaseSemiterminadoVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<BaseSemiterminadoVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<BaseSemiterminadoVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var basesemiterminado = await _context.BaseSemiterminado
                        .Where(x => !x.IsSoftDeleted && x.Enabled).ProjectToType<BaseSemiterminadoVm>().ToListAsync();
                    return basesemiterminado;
                }
            }


        }
    }
}
