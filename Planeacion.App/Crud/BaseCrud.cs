﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.MaquinaAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.Core.Extensions;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class BaseCrud
    {

        public class Create
        {
            public class BaseVm
            {
                public Guid Id { get; set; }
                public Guid CategoriaId { get; set; }
                public string NombreBase { get; set; }
                public string Nombre { get; set; }
            }
            public class Command:IRequest<BaseVm>
            {
                public Guid CategoriaId { get; set; }
                public string NombreBase { get; set; }
                public string Nombre { get; set; }

                public Guid UsuarioId { get; set; }
            }

            public class CommandHandler : IRequestHandler<Command, BaseVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<BaseVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var categoria = await _context.Categoria.Where(x => x.Id == command.CategoriaId).FirstOrDefaultAsync();
                    categoria.ThrowIfNull("Categoria");
                    var newbase = Base.New(command.Nombre,command.NombreBase,command.CategoriaId,command.UsuarioId);
                    await _context.Base.AddAsync(newbase);
                    await _context.SaveChangesAsync();
                    return newbase.Adapt<BaseVm>();
                }
            }
        }

        public class Index
        {
            public class BaseVm
            {
                public Guid Id { get; set; }
                public Guid CategoriaId { get; set; }
                public string CategoriaTipo { get; set; }

                public string NombreBase { get; set; }
                public string Nombre { get; set; }
            }
            public class Query : IRequest<List<BaseVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<BaseVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<BaseVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var bases = await _context.Base
                        .Where(x => !x.IsSoftDeleted && x.Enabled).ProjectToType<BaseVm>().ToListAsync();
                    return bases;
                }
            }
        }
    }
}
