﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.MaterialAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class IngredienteSemiterminadoCrud
    {
        public class Create
        {

            public class IngredienteSemiterminadoVm
            {
                public Guid Id { get; set; }
                public Guid SemiterminadoId { get; set; }
                public Guid IngredienteId { get; set; }
            }
            public class Command : IRequest<IngredienteSemiterminadoVm>
            {
                public Guid IngredienteId { get; set; }
                public Guid SemiterminadoId { get; set; }
                public Guid UsuarioId { get; set; }
            }
            public class CommandHandler : IRequestHandler<Command, IngredienteSemiterminadoVm>

            {
                private readonly Domain.Data.PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<IngredienteSemiterminadoVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var ingredientesemiterminado = IngredienteSemiterminado.New(command.IngredienteId, command.SemiterminadoId, command.UsuarioId);
                    await _context.IngredienteSemiterminado.AddAsync(ingredientesemiterminado);
                    await _context.SaveChangesAsync();
                    return ingredientesemiterminado.Adapt<IngredienteSemiterminadoVm>();
                }
            }
        }

        public class Index
        {
            public class IngredienteSemiterminadoVm
            {
                public Guid Id { get; set; }
                public Guid SemiterminadoId { get; set; }
                public Guid IngredienteId { get; set; }

            }
            public class Query : IRequest<List<IngredienteSemiterminadoVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<IngredienteSemiterminadoVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<IngredienteSemiterminadoVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var ingredientebase = await _context.IngredienteSemiterminado
                        .Where(x => !x.IsSoftDeleted && x.Enabled).ProjectToType<IngredienteSemiterminadoVm>().ToListAsync();
                    return ingredientebase;
                }
            }


        }

    }
}
