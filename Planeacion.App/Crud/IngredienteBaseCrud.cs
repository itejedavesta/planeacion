﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.MaterialAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class IngredienteBaseCrud
    {

        public class Create
        {

            public class IngredienteBaseVm
            {
                public Guid Id { get; set; }
                public Guid BaseId { get; set; }
                public Guid IngredienteId { get; set; }
            }
            public class Command : IRequest<IngredienteBaseVm>
            {
                public Guid IngredienteId { get; set; }
                public Guid BaseId { get; set; }
                public Guid UsuarioId { get; set; }
            }
            public class CommandHandler : IRequestHandler<Command, IngredienteBaseVm>

            {
                private readonly Domain.Data.PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<IngredienteBaseVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var ingredientebase = IngredienteBase.New(command.IngredienteId, command.BaseId, command.UsuarioId);
                    await _context.IngredienteBase.AddAsync(ingredientebase);
                    await _context.SaveChangesAsync();
                    return ingredientebase.Adapt<IngredienteBaseVm>();
                }
            }
        }

        public class Index
        {
            public class IngredienteBaseVm
            {
                public Guid Id { get; set; }
                public Guid BaseId { get; set; }
                public Guid IngredienteId { get; set; }

            }
            public class Query : IRequest<List<IngredienteBaseVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<IngredienteBaseVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<IngredienteBaseVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var ingredientebase = await _context.IngredienteBase
                        .Where(x => !x.IsSoftDeleted && x.Enabled).ProjectToType<IngredienteBaseVm>().ToListAsync();
                    return ingredientebase;
                }
            }


        }
    }
}
