﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.LineaAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.Core.App;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class TipoLineaCrud
    {

        public class Create
        {

            public class TipoLineaVm
            {
                public string Descripcion { get; set; }
                public Guid Id { get; set; }
            }
            public class Command:IRequest<TipoLineaVm>
            {
                public string Descripcion { get; set; }
                public Guid UsuarioId { get; set; }
            }
            public class CommandHandler : IRequestHandler<Command, TipoLineaVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<TipoLineaVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var newtipolinea = TipoLinea.New(command.Descripcion, command.UsuarioId);
                    await _validationManager.ValidateAndThrow(newtipolinea, RulesSet.Default);
                    await _context.TipoLinea.AddAsync(newtipolinea);
                    await _context.SaveChangesAsync();
                    return newtipolinea.Adapt<TipoLineaVm>();


                }
            }
        }


        public class Index
        {
            public class TipoLineaVm
            {
                public string Descripcion { get; set; }
                public Guid Id { get; set; }
            }

            public class Query:IRequest<List<TipoLineaVm>>
            {

            }

            public class QueryHandler : IRequestHandler<Query, List<TipoLineaVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<TipoLineaVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var tipolineas = await _context.TipoLinea
                        .Where(x => !x.IsSoftDeleted ).ProjectToType<TipoLineaVm>().ToListAsync();
                    return tipolineas;
                }
            }
        }
    }
}
