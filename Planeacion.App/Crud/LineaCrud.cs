﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.LineaAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.Core.Extensions;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class LineaCrud

    {

        public class Create
        {

            public class LineaVm
            {

                public Guid Id { get; set; }
                public string LineaDescripcion { get; set; }
                public string Turno { get; set; }
                public int Orden { get; set; }


                public Guid CreatedBy { get; set; }
            }
            public class Command : IRequest<LineaVm>
            {
                public string LineaDescripcion { get; set; }
                public string Turno { get; set; }
                public int Orden { get; set; }

                public Guid TipoLineaId { get; set; }
                public Guid UsuarioId { get; set; }
            }
            public class CommandHandler : IRequestHandler<Command, LineaVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<LineaVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var tipolinea = await _context.TipoLinea.Where(x => x.Id == command.TipoLineaId && !x.IsSoftDeleted).FirstOrDefaultAsync();
                    tipolinea.ThrowIfNull("TipoLinea");
                    var newlinea = Linea.New(command.LineaDescripcion, command.Turno, command.Orden, tipolinea.Id, command.UsuarioId);
                    await _context.Linea.AddAsync(newlinea);
                    await _context.SaveChangesAsync();
                    return newlinea.Adapt<LineaVm>();
                }
            }

        }
        public class LineaVm
        {

            public Guid Id { get; set; }
            public string LineaDescripcion { get; set; }
            public string Turno { get; set; }
            public int Orden { get; set; }


            public Guid CreatedBy { get; set; }
        }

        public class Index
        {
            public class Query:IRequest<List<LineaVm>>
            {

            }

            public class QueryHandler : IRequestHandler<   Query, List<LineaVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<LineaVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var lineas = await _context.Linea.Where(x => !x.IsSoftDeleted 
                    && x.Enabled).ProjectToType<LineaVm>().ToListAsync();
                    return lineas;

                }
            }

        }
    }
}
