﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.PlantaAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.Core.App;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class PlantaCrud

    {

        public class Create
        {

            public class PlantaVm
            {
                public Guid Id { get; set; }
                public string Nombre { get; set; }
                public Guid SitioPersonaId { get; set; }

            }
            public class Command:IRequest<PlantaVm>
            {

                public Guid UsuarioId { get; set; }
                public string Nombre { get; set; }
                public Guid SitioPersonaId { get; set; }

            }
            public class CommandHandler : IRequestHandler<Command, PlantaVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<PlantaVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var newplanta = Planta.New(command.Nombre,command.SitioPersonaId,command.UsuarioId);
                    await _validationManager.ValidateAndThrow(newplanta, RulesSet.Default);
                    await _context.Planta.AddAsync(newplanta);
                    await _context.SaveChangesAsync();
                    return newplanta.Adapt<PlantaVm>();


                }
            }

        }

        public class Index

        {
            public class PlantaVm
            {
                public Guid Id { get; set; }
                public string Nombre { get; set; }
                public Guid SitioPersonaId { get; set; }

            }

            public class Query : IRequest<List<PlantaVm>>
            {

            }
            public class QueryHandler : IRequestHandler<Query, List<PlantaVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<PlantaVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var newplantas = await _context.Planta
                        .Where(x => !x.IsSoftDeleted && x.Enabled).ProjectToType<PlantaVm>().ToListAsync();
                    return newplantas;
                }
            }
        }
    }
}
