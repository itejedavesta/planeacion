﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.MaterialAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.Core.Extensions;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
public class IngredienteCrud
    {

    public class Create
        {


            public class IngredienteVm
            {
                public Guid Id { get; set; }

                public Guid CategoriaId { get; set; }
                public string Descripcion { get; set; }
                public Guid SkuId { get; set; }

            }
            public class Command:IRequest<IngredienteVm>
            {
                public Guid CategoriaId { get;  set; }
                public string Descripcion { get; set; }
                public Guid SkuId { get; set; }
                public Guid UsuarioId { get; set; }

            }
            public class CommandHandler : IRequestHandler<Command, IngredienteVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<IngredienteVm> Handle(Command command, CancellationToken cancellationToken)
                {

                    var categoria = await _context.Categoria.Where(x => x.Id == command.CategoriaId && x.Enabled && !x.IsSoftDeleted).FirstOrDefaultAsync();
                    categoria.ThrowIfNull("Categoria");
                    var newingrediente = Ingrediente.New(command.Descripcion, command.SkuId, command.CategoriaId, command.UsuarioId);
                    await _validationManager.ValidateAndThrow(newingrediente, RulesSet.Default);
                    await _context.Ingrediente.AddAsync(newingrediente);
                    await _context.SaveChangesAsync();

                    return newingrediente.Adapt<IngredienteVm>(); ;

                }
            }

        }

        public class Index
        {
            public class IngredienteVm
            {
                public Guid Id { get; set; }

                public Guid CategoriaId { get; set; }
                public string CategoriaTipo { get; set; }
                public string Descripcion { get; set; }
                public Guid SkuId { get; set; }

            }
            public class Query : IRequest<List<IngredienteVm>>
            {

            }

            public class QueryHandler : IRequestHandler<Query, List<IngredienteVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<IngredienteVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var lineas = await _context.Ingrediente.Where(x => !x.IsSoftDeleted
                    && x.Enabled).ProjectToType<IngredienteVm>().ToListAsync();
                    return lineas;

                }
            }

        }

    }

}

