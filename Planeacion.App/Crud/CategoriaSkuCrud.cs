﻿using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Planeacion.App.Domain.Data;
using Planeacion.App.Domain.Model.PlantaAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vesta2.FluentValidation;

namespace Planeacion.App.Crud
{
    public class CategoriaSkuCrud
    {

        public class Create
        {

            public class CategoriaSkuVm
            {
                public Guid Id { get; set; }
                public string Tipo { get; set; }
            }
            public class Command:IRequest<CategoriaSkuVm>
            {

                public string Tipo { get; set; }
                public Guid UsuarioId { get; set; }
            }


            public class CommandHandler : IRequestHandler<Command, CategoriaSkuVm>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public CommandHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async System.Threading.Tasks.Task<CategoriaSkuVm> Handle(Command command, CancellationToken cancellationToken)
                {
                    var newcategoria = CategoriaSKU.New(command.Tipo, command.UsuarioId);
                    await _validationManager.ValidateAndThrow(newcategoria);
                    await _context.CategoriaSKU.AddAsync(newcategoria);
                    await _context.SaveChangesAsync();
                    return newcategoria.Adapt<CategoriaSkuVm>();


                }
            }


        }

        public class Index
        {
            public class CategoriaSkuVm
            {
                public Guid Id { get; set; }
                public string Tipo { get; set; }
            }
            
            public class Query : IRequest<List<CategoriaSkuVm>>
            {

            }

            public class QueryHandler : IRequestHandler<Query, List<CategoriaSkuVm>>

            {
                private readonly PlaneacionDbContext _context;
                private readonly IValidationManager _validationManager;

                public QueryHandler(PlaneacionDbContext context, IValidationManager ivaValidationManager)
                {
                    _context = context;
                    _validationManager = ivaValidationManager;
                }

                public async Task<List<CategoriaSkuVm>> Handle(Query query, CancellationToken cancellationToken)
                {
                    var categorias = await _context.CategoriaSKU
                        .Where(x => !x.IsSoftDeleted).ProjectToType<CategoriaSkuVm>().ToListAsync();
                    return categorias;
                }
            }
        }
    }
}
