﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Text;

namespace Planeacion.App
{
    public class PlaneacionConfiguracion
    {
        public static void Configuracion()
        {
            var localAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            TypeAdapterConfig.GlobalSettings.Scan(localAssembly);
            TypeAdapterConfig.GlobalSettings.AllowImplicitDestinationInheritance = true;
        }
    }
}
