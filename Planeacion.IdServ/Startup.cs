using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Planeacion.App.Dto;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Swagger;
using Vesta2.Core.Config;
using Vesta2.App.Core.Client;
using StackExchange.Profiling.SqlFormatters;
using Vesta2.SequentialDb.Ef2;
using Planeacion.App.Domain.Data;
using Vesta2.AspNetCore2.Config;

namespace Planeacion.IdServ
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //////ESTE ES EL ULTIMO EN EL QUE ESTOY TRABAJANDO
            var connString = Configuration.GetConnectionString("DefaultConnection");
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Analisis de Red",
                    Version = "v1",
                    Description = "Analisis de Red",
                    Contact = new Swashbuckle.AspNetCore.Swagger.Contact { Name = "Sofia Tejeda", Email = "itejeda@grupovesta.net", Url = "" }
                });

                c.CustomSchemaIds((type) => type.FullName);
            });

            services.AddDbContextPool<PlaneacionDbContext>(
                options => options.UseSqlServer(connString));

            services.AddDbContext<EfSequentialDbContext>(options => options.UseSqlServer(connString));

            Planeacion.App.PlaneacionConfiguracion.Configuracion();
            services.AddCustomAspNet(options =>
            {
                options.ProjectConfiguration = Configuration;
                options.ProjectOwner = SharedAppConfigClient.Vesta;
                options.ProjectClient = AspNetCoreClient.WebApi;

                options.RegisterTheseAppSettings(AppSettingsSection.AppWide, AppSettingsSection.Email);
                //options.RegisterAppSettings = new[] { AppSettingsSection.AppWide, AppSettingsSection.Email };

                options.LoggingPipe.ConnString = connString;
                options.LoggingPipe.Enable = false;
                options.LoggingPipe.EnableDbContextPool = true;

                options.Pipeline.RegisterCqrs = true;
                options.Pipeline.RegisterFluentValidation = true;
                options.Pipeline.RegisterConventions = true;
                //options.Pipeline.RegisterFromAssembliesWithTypes = new List<Type>
                //{
                //    typeof(AnalisisDeRedDbContext)
                //};
                options.Pipeline.RegisterFromAssembliesWithTypes(typeof(PlaneacionDbContext));

                options.IdentityServer.SistemaId = "";
                options.IdentityServer.ClientId = "";
                options.IdentityServer.ClientSecret = "";
                //options.IdentityServer.AuthorizeTheseWebApis = new[] { "" };
                options.IdentityServer.AuthorizeTheseWebApis();
                options.IdentityServer.EnableAuthentication = false;
                options.IdentityServer.EnableAuthorization = false;
                ;
                //options.IdentityServer. = false;
            });
            services.AddCors(options => options.AddPolicy("CorsPolicy",
          builder =>
          {
              builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();


          }));
            services.AddMiniProfiler(s =>
            {
                s.RouteBasePath = "/profiler";// http://localhost:53244/profiler/results to get latest report
                s.SqlFormatter = new SqlServerFormatter();
            }).AddEntityFramework();
            services.Configure<UrlSettings>(Configuration.GetSection("UrlSettings"));
            return services.ToCustomServiceProvider();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger(c =>
            {

            });
            // app.UseCors("CorsPolicy");
            app.UseCors("CorsPolicy");
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Planeacion");
            });
            if (env.IsDevelopment())
            {
                //app.UseBrowserLink();
                app.UseDeveloperExceptionPage();

            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc();
        }

    }
}
