﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Planeacion.App.Crud;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Planeacion.IdServ.Controllers.Crud
{
    [Route("api/SemiterminadoCrudApi/[action]")]

    public class SemiterminadoCrudApiController:Controller
    {
        private readonly IMediator _imediator;
        public SemiterminadoCrudApiController(IMediator imediator
         )
        {
            _imediator = imediator;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SemiterminadoCrud.Create.Command command)
        {
            var resp = await _imediator.Send(command);
            return Ok(resp);
        }
        [HttpPost]
        public async Task<IActionResult> Index([FromBody] SemiterminadoCrud.Index.Query query)
        {
            var resp = await _imediator.Send(query);
            return Ok(resp);
        }

    }
}
