﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Planeacion.App.Crud;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Planeacion.IdServ.Controllers.Crud
{
    [Route("api/BaseCrudApi/[action]")]
    public class BaseCrudApiController:Controller
    {
        private readonly IMediator _imediator;
        public BaseCrudApiController(IMediator imediator
         )
        {
            _imediator = imediator;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] BaseCrud.Create.Command command)
        {
            var resp = await _imediator.Send(command);
            return Ok(resp);
        }
        [HttpPost]
        public async Task<IActionResult> Index([FromBody] BaseCrud.Index.Query query)
        {
            var resp = await _imediator.Send(query);
            return Ok(resp);
        }

    }
}
