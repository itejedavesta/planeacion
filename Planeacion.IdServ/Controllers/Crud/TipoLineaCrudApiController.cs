﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Planeacion.App.Crud;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Planeacion.IdServ.Controllers.Crud
{
    [Route("api/TipoLineaCrudApi/[action]")]

    public class TipoLineaCrudApiController:Controller
    {
        private readonly IMediator _imediator;
        public TipoLineaCrudApiController(IMediator imediator
         )
        {
            _imediator = imediator;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TipoLineaCrud.Create.Command command)
        {
            var resp = await _imediator.Send(command);
            return Ok(resp);
        }
        [HttpPost]
        public async Task<IActionResult> Index([FromBody] TipoLineaCrud.Index.Query query)
        {
            var resp = await _imediator.Send(query);
            return Ok(resp);
        }

    }
}
